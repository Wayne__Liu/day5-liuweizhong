## **O**：

- First, our team conducted a code review. 
- take pakingLot as an example to do a lot of TDD and Java OO practice.
- I have consolidated many of the knowledge I have learned in previous classes during my practice, such as polymorphism, Java OO, and TDD.



## **R：**

- Then, today's exercise involves the knowledge of java exceptions. I don't know much about this knowledge, so I can strengthen my study. I didn't keep up with the teacher in some steps in class, which resulted in a period of difficulty.


## **I：**

- exciting


## **D：**

- More time can be spent researching design patterns and best practices to combine OOP, TDD, and design patterns.
