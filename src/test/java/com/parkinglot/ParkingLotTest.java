package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class ParkingLotTest {

    private ParkingLot parkingLot;

    @BeforeEach
    public void setup() {
        parkingLot = new ParkingLot();
    }

    @Test
    public void should_return_a_parking_ticket_when_park_given_a_car_and_a_parking_lot() {
        Car car = new Car();
        Ticket ticket = parkingLot.park(car);
        Assertions.assertNotNull(ticket);
    }

    @Test void should_return_the_car_when_park_given_a_parking_ticket_and_a_parked_car_and_a_parking_lot(){
        Car car = new Car();
        Ticket ticket = parkingLot.park(car);
        Car fetchedCar = parkingLot.fetch(ticket);

        Assertions.assertEquals(car, fetchedCar);
    }

    @Test
    public void should_return_the_right_car_when_park_given_two_parking_ticket_and_two_parked_car_and_a_parking_lot(){
        Car car1 = new Car();
        Car car2 = new Car();

        Ticket ticket1 = parkingLot.park(car1);
        Ticket ticket2 = parkingLot.park(car2);

        Car fetchedCar1 = parkingLot.fetch(ticket1);
        Car fetchedCar2 = parkingLot.fetch(ticket2);

        Assertions.assertEquals(car1, fetchedCar1);
        Assertions.assertEquals(car2, fetchedCar2);
    }

    @Test
    public void should_return_error_message_when_fetch_the_car_given_a_wrong_parking_ticket_and_a_parking_lot() {
        Ticket wrongTicket = new Ticket();

//        Car fetchedCar = parkingLot.fetch(wrongTicket);
//
//        Assertions.assertNull(fetchedCar);
        UnexpectedProjectTypeException unexpectedProjectTypeException = assertThrows(UnexpectedProjectTypeException.class, () -> parkingLot.fetch(wrongTicket));
        Assertions.assertEquals("Unrecognized parking ticket", unexpectedProjectTypeException.getMessage());
    }

    @Test
    public void should_return_error_message_when_fetch_the_car_given_a_used_parking_ticket_and_a_parking_lot() {
        Car car = new Car();
        Ticket ticket = parkingLot.park(car);
        parkingLot.fetch(ticket);

//        Car fetchedCar = parkingLot.fetch(ticket);
//
//        Assertions.assertNull(fetchedCar);
        UnexpectedProjectTypeException unexpectedProjectTypeException = assertThrows(UnexpectedProjectTypeException.class, () -> parkingLot.fetch(ticket));
        Assertions.assertEquals("Unrecognized parking ticket", unexpectedProjectTypeException.getMessage());
    }

    @Test
    public void should_return_error_message_when_park_the_car_given_a_parking_lot_without_any_position() {
        for (int i = 0; i < 10; i++) {
            Car car = new Car();
            parkingLot.park(car);
        }

        Car extraCar = new Car();
//        Ticket ticket = parkingLot.park(extraCar);

//        Assertions.assertNull(ticket);
        UnexpectedProjectTypeException unexpectedProjectTypeException = assertThrows(UnexpectedProjectTypeException.class, () -> parkingLot.park(extraCar));
        Assertions.assertEquals("No available position", unexpectedProjectTypeException.getMessage());
    }

}
