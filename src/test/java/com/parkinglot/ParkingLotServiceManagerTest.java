package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ParkingLotServiceManagerTest {

    @Test
    public void should_return_manager_parkingBoys_when_add_to_parkingBoys_given_and_a_standard_parking_boy_and_a_manager() {
        ParkingLot parkingLot = new ParkingLot();
        List<ParkingLot> parkingLots = Collections.singletonList(parkingLot);

        ParkingLotServiceManager manager = new ParkingLotServiceManager(parkingLots);

        StandardParkingBoy parkingBoy = new StandardParkingBoy(parkingLots);

        manager.addParkingBoy(parkingBoy);

        Assertions.assertTrue(manager.getParkingBoys().contains(parkingBoy));
    }
    @Test
    public void should_return_a_parking_ticket_when_park_by_the_specified_parking_boy_given_two_parking_lot_and_a_car_and_a_smart_parking_boy_and_a_manager() {
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();

        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);

        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots);
        ParkingLotServiceManager manager = new ParkingLotServiceManager(parkingLots);

        manager.addParkingBoy(smartParkingBoy);

        Car car = new Car();

        Ticket ticket = manager.parkByParkingBoy(smartParkingBoy, car);

        Car fetchedCar = parkingLot1.fetch(ticket);
        Assertions.assertEquals(car, fetchedCar);
    }

    @Test
    public void should_return_error_message_when_park_by_the_specified_parking_boy_withAdd_given_a_parking_lot_and_a_car_and_a_standard_parking_boy_and_a_manager() {
        ParkingLot parkingLot = new ParkingLot();
        List<ParkingLot> parkingLots = Collections.singletonList(parkingLot);

        StandardParkingBoy parkingBoy = new StandardParkingBoy(parkingLots);
        ParkingLotServiceManager manager = new ParkingLotServiceManager(parkingLots);

        Car car = new Car();

        try {
            manager.parkByParkingBoy(parkingBoy, car);
            Assertions.fail("Should throw IllegalArgumentException.");
        } catch (IllegalArgumentException e) {
            Assertions.assertEquals("Invalid parking boy.", e.getMessage());
        }
    }

    @Test
    public void should_return_a_right_car_when_fetch_by_manager_given_a_car_and_parkLot_and_a_manager() {
        ParkingLot parkingLot = new ParkingLot();
        List<ParkingLot> parkingLots = Collections.singletonList(parkingLot);

        ParkingLotServiceManager manager = new ParkingLotServiceManager(parkingLots);

        Car car = new Car();

        Ticket ticket = manager.park(car);

        Car fetchedCar = manager.fetch(ticket);
        Assertions.assertEquals(car, fetchedCar);
    }
}
