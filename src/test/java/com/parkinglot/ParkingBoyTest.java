package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;


public class ParkingBoyTest {
    private ParkingBoy parkingBoy;

    @BeforeEach
    public void setUp() {
        parkingBoy = new ParkingBoy();
    }

    @Test
    public void should_return_a_parking_ticket_when_park_given_a_car_and_a_parking_lot_and_a_standard_parking_boy() {
        Car car = new Car();

        Ticket ticket = parkingBoy.park(car);

        Assertions.assertNotNull(ticket);
    }

    @Test
    public void should_return_a_car_when_fetch_given_a_parking_ticket_and_a_parking_lot_and_a_standard_parking_boy() {
        Car car = new Car();

        Ticket ticket = parkingBoy.park(car);
        Car fetch = parkingBoy.fetch(ticket);

        Assertions.assertEquals(car,fetch);
    }

    @Test
    public void should_return_two_car_with_each_ticket_when_fetch_given_two_parking_ticket_and_a_parking_lot_and_two_parked_car_and_a_standard_parking_boy() {
        Car car1 = new Car();
        Car car2 = new Car();

        Ticket ticket1 = parkingBoy.park(car1);
        Ticket ticket2 = parkingBoy.park(car2);
        Car fetch1 = parkingBoy.fetch(ticket1);
        Car fetch2 = parkingBoy.fetch(ticket2);

        Assertions.assertEquals(car1,fetch1);
        Assertions.assertEquals(car2,fetch2);
    }

    @Test
    public void should_return_error_message_when_fetch_the_car_given_a_wrong_parking_ticket_and_a_parking_lot_and_a_standard_parking_boy() {
        Ticket wrongTicket = new Ticket();

        UnexpectedProjectTypeException unexpectedProjectTypeException = assertThrows(UnexpectedProjectTypeException.class, () -> parkingBoy.fetch(wrongTicket));
        Assertions.assertEquals("Unrecognized parking ticket", unexpectedProjectTypeException.getMessage());
    }

    @Test
    public void should_return_error_message_when_fetch_the_car_given_a_used_parking_ticket_and_a_parking_lot_and_a_standard_parking_boy() {
        Car car = new Car();
        Ticket ticket = parkingBoy.park(car);
        parkingBoy.fetch(ticket);

        UnexpectedProjectTypeException unexpectedProjectTypeException = assertThrows(UnexpectedProjectTypeException.class, () -> parkingBoy.fetch(ticket));
        Assertions.assertEquals("Unrecognized parking ticket", unexpectedProjectTypeException.getMessage());
    }

    @Test
    public void should_return_error_message_when_park_the_car_given_a_parking_lot_without_any_position_and_a_car_and_a_standard_parking_boy() {
        for (int i = 0; i < 10; i++) {
            Car car = new Car();
            parkingBoy.park(car);
        }
        Car extraCar = new Car();
        UnexpectedProjectTypeException unexpectedProjectTypeException = assertThrows(UnexpectedProjectTypeException.class, () -> parkingBoy.park(extraCar));
        Assertions.assertEquals("No available position", unexpectedProjectTypeException.getMessage());
    }
}
