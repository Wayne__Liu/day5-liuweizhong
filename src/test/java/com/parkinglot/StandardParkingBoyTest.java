package com.parkinglot;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;


public class StandardParkingBoyTest {
    private ParkingLot parkingLot1;
    private ParkingLot parkingLot2;
    private StandardParkingBoy standardParkingBoy;
    private Car car;

    @BeforeEach
    public void setUp() {
        parkingLot1 = new ParkingLot();
        parkingLot2 = new ParkingLot();
        standardParkingBoy = new StandardParkingBoy(List.of(parkingLot1, parkingLot2));
        car = new Car();
    }

    @Test
    public void should_return_a_parking_ticket_when_park_given_a_car_and_two_parking_lot_with_available_position_and_a_standard_parking_boy() {
        Ticket ticket = standardParkingBoy.park(car);

        Assertions.assertEquals(car, parkingLot1.fetch(ticket));
    }

    @Test
    public void should_return_a_parking_ticket_by_second_park_when_park_given_a_car_and_two_parking_lot_with_full_the_first_park_and_a_standard_parking_boy() {
        for (int i = 0; i < 10; i++) {
            parkingLot1.park(new Car());
        }

        Ticket ticket = standardParkingBoy.park(car);

        Assertions.assertEquals(car, parkingLot2.fetch(ticket));
    }

    @Test
    public void should_return_the_right_car_with_each_when_fetch_given_two_parking_lot_both_with_a_parked_car_and_two_parking_ticket_and_a_standard_parking_boy() {
        for (int i = 0; i < 9; i++) {
            Car car = new Car();
            standardParkingBoy.park(car);
        }
        Car car1 = new Car();
        Car car2 = new Car();

        Ticket ticket1 = standardParkingBoy.park(car1);
        Ticket ticket2 = standardParkingBoy.park(car2);

        Assertions.assertEquals(car1, standardParkingBoy.fetch(ticket1));
        Assertions.assertEquals(car2, standardParkingBoy.fetch(ticket2));
    }

    @Test
    public void should_return_error_message_when_fetch_the_car_given_a_unrecognized_parking_ticket_and_two_parking_lot_and_a_standard_parking_boy() {
        Ticket wrongTicket = new Ticket();

        UnexpectedProjectTypeException unexpectedProjectTypeException = assertThrows(UnexpectedProjectTypeException.class, () -> standardParkingBoy.fetch(wrongTicket));
        Assertions.assertEquals("Unrecognized parking ticket", unexpectedProjectTypeException.getMessage());
    }

    @Test
    public void should_return_error_message_when_fetch_the_car_given_a_used_parking_ticket_and_two_parking_lot_and_a_standard_parking_boy() {
        Car car = new Car();
        Ticket ticket = standardParkingBoy.park(car);
        standardParkingBoy.fetch(ticket);

        UnexpectedProjectTypeException unexpectedProjectTypeException = assertThrows(UnexpectedProjectTypeException.class, () -> standardParkingBoy.fetch(ticket));
        Assertions.assertEquals("Unrecognized parking ticket", unexpectedProjectTypeException.getMessage());
    }

    @Test
    public void should_return_error_message_when_park_the_car_given_two_parking_lot_without_any_position_and_a_car_and_a_standard_parking_boy() {
        for (int i = 0; i < 20; i++) {
            Car car = new Car();
            standardParkingBoy.park(car);
        }
        Car extraCar = new Car();
        UnexpectedProjectTypeException unexpectedProjectTypeException = assertThrows(UnexpectedProjectTypeException.class, () -> standardParkingBoy.park(extraCar));
        Assertions.assertEquals("No available position", unexpectedProjectTypeException.getMessage());
    }
}
