package com.parkinglot;

import java.util.HashMap;
import java.util.Map;

public class ParkingLot {

    private int capacity;
    private Map<Ticket, Car> parkedCars;

    public ParkingLot() {
        this.capacity = 10;
        this.parkedCars = new HashMap<>();
    }

    public Map<Ticket, Car> getParkedCars() {
        return parkedCars;
    }

    public int getCapacity() {
        return capacity;
    }

    public Ticket park(Car car) {
        if(!hasAvailablePosition()) throw new UnexpectedProjectTypeException("No available position");
        Ticket ticket = new Ticket();
        parkedCars.put(ticket, car);
        return ticket;
    }

    public Car fetch(Ticket ticket) {
        if (ticket == null || !parkedCars.containsKey(ticket)) {
            throw new UnexpectedProjectTypeException("Unrecognized parking ticket");
        }

        Car car = parkedCars.get(ticket);
        parkedCars.remove(ticket);
        return car;
    }

    public boolean hasAvailablePosition() {
        return parkedCars.size() != capacity;
    }
}
