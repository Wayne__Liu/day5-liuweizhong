package com.parkinglot;

import java.util.ArrayList;
import java.util.List;

public class SmartParkingBoy extends StandardParkingBoy{
    public SmartParkingBoy(List<ParkingLot> parkingLots) {
        super(parkingLots);
    }

    public Ticket park(Car car) {
        ParkingLot parkingLot1 = parkingLots.get(currentIndex);
        updateCurrentIndex();
        ParkingLot parkingLot2 = parkingLots.get(currentIndex);
        if (parkingLot1.hasAvailablePosition() || parkingLot1.hasAvailablePosition()) {
            if (parkingLot1.getParkedCars().size() <= parkingLot2.getParkedCars().size()) {
                return parkingLot1.park(car);
            } else {
                return parkingLot2.park(car);
            }
        }
        throw new UnexpectedProjectTypeException("No available position");
    }
}
