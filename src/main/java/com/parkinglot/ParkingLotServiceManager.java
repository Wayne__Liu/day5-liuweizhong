package com.parkinglot;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ParkingLotServiceManager extends StandardParkingBoy{

    private List<ParkingBoy> parkingBoys;

    public ParkingLotServiceManager(List<ParkingLot> parkingLots) {
        super(parkingLots);
        this.parkingBoys = new ArrayList<>();
    }

    public void addParkingBoy(ParkingBoy parkingBoy) {
        parkingBoys.add(parkingBoy);
    }

    public Ticket parkByParkingBoy(ParkingBoy parkingBoy, Car car) {
        if (parkingBoys.contains(parkingBoy)) {
            return parkingBoy.park(car);
        } else {
            throw new IllegalArgumentException("Invalid parking boy.");
        }
    }

    public Car fetchByParkingBoy(ParkingBoy parkingBoy, Ticket ticket) {
        if (parkingBoys.contains(parkingBoy)) {
            return parkingBoy.fetch(ticket);
        } else {
            throw new IllegalArgumentException("Invalid parking boy.");
        }
    }

    @Override
    public Ticket park(Car car) {
        return super.park(car);
    }

    @Override
    public Car fetch(Ticket ticket) {
        return super.fetch(ticket);
    }

    public List<ParkingBoy> getParkingBoys() {
        return parkingBoys;
    }
}
