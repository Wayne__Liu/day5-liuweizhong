package com.parkinglot;

import java.util.ArrayList;
import java.util.List;

public class StandardParkingBoy extends ParkingBoy{
    protected final List<ParkingLot> parkingLots;
    protected int currentIndex;

    public StandardParkingBoy(List<ParkingLot> parkingLots) {
        this.parkingLots = new ArrayList<>(parkingLots);
        this.currentIndex = 0;
    }

    public Ticket park(Car car) {
        for (int i = 0; i < parkingLots.size(); i++) {
            ParkingLot parkingLot = parkingLots.get(currentIndex);
            if (parkingLot.hasAvailablePosition()) {
                return parkingLot.park(car);
            }
            updateCurrentIndex();
        }
        throw new UnexpectedProjectTypeException("No available position");
    }
    public Car fetch(Ticket ticket) {
        if (ticket == null) {
            return null;
        }
        for (ParkingLot parkingLot : parkingLots) {
            try {
                Car car = parkingLot.fetch(ticket);
                if (car != null) {
                    return car;
                }
            }catch (UnexpectedProjectTypeException ignored){

            }
        }
        throw new UnexpectedProjectTypeException("Unrecognized parking ticket");
    }

    protected void updateCurrentIndex() {
        currentIndex = (currentIndex + 1) % parkingLots.size();
    }
}
