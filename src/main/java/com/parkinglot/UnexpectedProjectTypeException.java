package com.parkinglot;

public class UnexpectedProjectTypeException extends RuntimeException {
    public UnexpectedProjectTypeException(String message) {
        super(message);
    }
}
